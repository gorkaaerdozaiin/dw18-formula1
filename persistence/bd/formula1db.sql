CREATE DATABASE IF NOT EXISTS formula1db;

USE formula1db;

CREATE TABLE IF NOT EXISTS car (
	idCar int not null AUTO_INCREMENT,
	model varchar(255),
        pilot varchar(30),
	team varchar(40),
	image varchar(255),
	PRIMARY KEY( idCar )
);

INSERT INTO `car` VALUES (1,'Red Bull RB16B','Max Verstappen','Red Bull Racing Honda','https://img.redbull.com/images/c_fill,w_720,h_365,g_auto,f_auto,q_auto/redbullcom/2021/6/14/vofufzmkztcg8vmat1j5/rb16b_2020');

INSERT INTO `car` VALUES (2,'Mercedes W12','Lewis Hamilton','Mercedes-AMG Petronas','https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/mercedes-w12-09-1614682041.jpg');