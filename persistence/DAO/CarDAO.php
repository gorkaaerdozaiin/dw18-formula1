<?php

//dirname(__FILE__) Es el directorio del archivo actual
require_once(dirname(__FILE__) . '/../conf/PersistentManager.php');

class CarDAO {

    //Se define una constante con el nombre de la tabla
    const CAR_TABLE = 'car';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . CarDAO::CAR_TABLE;
        $result = mysqli_query($this->conn, $query);
        $cars = array();
        while ($carBD = mysqli_fetch_array($result)) {

            $car = new Car();
            $car->setIdCar($carBD["idCar"]);
            $car->setModel($carBD["model"]);
            $car->setPilot($carBD["pilot"]);
            $car->setTeam($carBD["team"]);
            $car->setImage($carBD["image"]);
            
            array_push($cars, $car);
        }
        return $cars;
    }

    public function insert($car) {
        $query = "INSERT INTO " . CarDAO::CAR_TABLE .
                " (model, pilot, team, image) VALUES(?,?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $model = $car->getModel();
        $pilot = $car->getPilot();
        $team = $car->getTeam();
        $image = $car->getImage();
        
        mysqli_stmt_bind_param($stmt, 'ssss', $model, $pilot, $team, $image);
        return $stmt->execute();
    }

    public function selectById($id) {
        $query = "SELECT model, pilot, team, image FROM " . CarDAO::CAR_TABLE . " WHERE idCar=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $model, $pilot, $team, $image);

        $car = new Car();
        while (mysqli_stmt_fetch($stmt)) {
            $car->setIdCar($id);
            $car->setModel($model);
            $car->setPilot($pilot);
            $car->setTeam($team);
            $car->setImage($image);
       }

        return $car;
    }

    public function update($car) {
        $query = "UPDATE " . CarDAO::CAR_TABLE .
                " SET model=?, pilot=?, team=?, image=?"
                . " WHERE idCar=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $model = $car->getModel();
        $pilot= $car->getPilot();
        $team = $car->getTeam();
        $image = $car->getImage();
        $id = $car->getIdCar();
        mysqli_stmt_bind_param($stmt, 'ssssi', $model, $pilot, $team, $image, $id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . CarDAO::CAR_TABLE . " WHERE idCar=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

        
}

?>
