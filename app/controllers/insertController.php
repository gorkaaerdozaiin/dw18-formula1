<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/CarDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Car.php');
require_once(dirname(__FILE__) . '/../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    $model = ValidationsRules::test_input($_POST["model"]);
    $pilot = ValidationsRules::test_input($_POST["pilot"]);
    $team = ValidationsRules::test_input($_POST["team"]);
    $image = ValidationsRules::test_input($_POST["image"]);
    // TODOD hacer uso de los valores validados 
    $car = new Car();
    $car->setModel($_POST["model"]);
    $car->setPilot($_POST["pilot"]);
    $car->setTeam($_POST["team"]);
    $car->setImage($_POST["image"]);

    //Creamos un objeto CreatureDAO para hacer las llamadas a la BD
    $carDAO = new CarDAO();
    $carDAO->insert($car);
    
    header('Location: ../../index.php');
    
}
?>

