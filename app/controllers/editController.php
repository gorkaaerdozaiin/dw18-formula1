<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/CarDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Car.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $model = $_POST["model"];
    $pilot = $_POST["pilot"];
    $team = $_POST["team"];
    $image = $_POST["image"];

    $car = new Car();
    $car->setIdCar($id);
    $car->setModel($model);
    $car->setPilot($pilot);
    $car->setTeam($team);
    $car->setImage($image);

    $carDAO = new CarDAO();
    $carDAO->update($car);

    header('Location: ../../index.php');
}

?>

