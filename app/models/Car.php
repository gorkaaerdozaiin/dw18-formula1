<?php

class Car {

    private $idCar;
    private $model;
    private $pilot;
    private $team;
    private $image;

    public function __construct() {
        
    }

    public function getIdCar() {
        return $this->idCar;
    }

    public function getModel() {
        return $this->model;
    }

    public function getPilot() {
        return $this->pilot;
    }

    public  function getTeam() {
        return $this->team;
    }
    
    public function getImage() {
        return $this->image;
    }

    public function setIdCar($idCar) {
        $this->idCar = $idCar;
    }

    public function setModel($model) {
        $this->model = $model;
    }

    public function setPilot($pilot) {
        $this->pilot = $pilot;
    }

    function setTeam($team) {
        $this->team = $team;
    }
    
    function setImage($image) {
        $this->image = $image;
    }

//Función para pintar cada criatura
    function car2HTML() {
        $result = '<div class=" col-md-4 ">';
         $result .= '<div class="card ">';
          $result .= ' <img class="card-img-top rounded mx-auto d-block avatar" src='.$this->getImage().' alt="Card image cap">';
            $result .= '<div class="card-block">';
                $result .= '<h2 class="card-title">' . $this->getModel() . '</h2>';
                $result .= '<ul class="list-group list-group-flush">
                <li class="list-group-item">Piloto => ' . $this->getPilot() . '</li>
                <li class="list-group-item">Escudería => ' . $this->getTeam() . '</li>
                </ul>';                    
             $result .= '</div>';
             $result .= ' <div  class=" btn-group card-footer" role="group">';
                $result .= '<a type="button" class="btn btn-outline-secondary" href="app/views/detail.php?id='.$this->getIdCar().'">Detalles</a>';
                $result .= '<a type="button" class="btn btn-outline-success" href="app/views/edit.php?id='.$this->getIdCar().'">Modificar</a> ';
                $result .= '<a type="button" class="btn btn-outline-danger" href="app/controllers/deleteController.php?id='.$this->getIdCar().'">Borrar</a> ';
            $result .= ' </div>';
         $result .= '</div>';
     $result .= '</div>';
        
        
        return $result;
    }
    
    
}
