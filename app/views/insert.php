<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gestión de Coches</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">


    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-light navbar-fixed-top navbar-expand-md bg-faded" role="navigation" style="background-color: #fafafa;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="../../index.php"> <img src="../../assets/img/small-logo.png" alt="" width="100px" class="img-fluid rounded"></a>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                  <ul class="navbar-nav mr-auto ">
                    <li class="nav-item active">
                      <a type="button" class="btn btn-dark " href="#">Volver al inicio</a>
                    </li>
                  </ul>
                    
                </div>
              </nav>
        <!-- Page Content -->
         <div class="container">
            <form class="form-horizontal" method="post" action="../controllers/insertController.php">
                
                <div class="form-group">
                    <label for="model" class="col-sm-2 control-label">Modelo</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="model" id="model" placeholder="Modelo" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pilot" class="col-sm-2 control-label">Piloto</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="pilot" name="pilot" placeholder="Piloto" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="team" class="col-sm-2 control-label">Escudería</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="team" name="team" placeholder="Escudería" value="">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="image" class="col-sm-2 control-label">Imagen</label>
                    <div class="col-sm-10">
                        <input type="url" class="form-control" id="image" name="image" placeholder="Imagen" value="">
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-outline-dark">Insertar</button>
                    </div>
                </div>
            </form>

            <!-- Footer -->
           <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; G. E. 2021</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../../assets/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../assets/js/bootstrap.min.js"></script>
    </body>

</html>


